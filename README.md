CurrencyFrom.java contains enums for CurrencyFrom scenario
CurrencyTo.java contains enums for CurrencyTo scenario
Monetary.java is a java bean with getters/setters to CurrencyTo/CurrencyFrom and corresponding string representations
IndexController.java contains two RestAPIs 
1. GetMapping to populate the thymeleaf with Monetary values
2. PostMapping to compute the conversion of CurrencyFrom to CurrencyTo scenario