package de.c24.finacc.klt.web.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import de.c24.finacc.klt.entity.CurrencyFrom;
import de.c24.finacc.klt.entity.CurrencyTo;
import de.c24.finacc.klt.entity.Monetary;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

/**
 * IndexController
 */
@Controller
@RequestMapping({"/", "/index"})
public class IndexController {

    /**
     * Index endpoint to show the index page
     *
     * @param model Spring's view model
     * @return view name
     */
    @GetMapping
    public String index(Model model) {
        model.addAttribute("title", "Karten&Konten KLT");
        model.addAttribute("welcome", "Welcome to Check24");
        model.addAttribute("applicationTitle", "Check24 Currency Converter");
        model.addAttribute("monetary", new Monetary());
        model.addAttribute("placeholder", 0);
        return "index";
    }

    @PostMapping
    public String convert(@ModelAttribute  Monetary monetary, Model model) throws IOException, JSONException, InterruptedException {
        Monetary currency = (Monetary) model.getAttribute("monetary");
        CurrencyFrom currencyFrom = currency.getCurrencyFrom();
        CurrencyTo currencyTo = currency.getCurrencyTo();
        String tfCurrencyFrom = currency.getTfCurrencyFrom();
        Double quantity = Double.parseDouble(tfCurrencyFrom);
        String amount = currency.getAmount();

        Response response = getResponse(currencyFrom);
        String jsonData = response.body().string();
        JSONObject jObject = new JSONObject(jsonData);
        JSONObject value = (JSONObject) jObject.get("rates");
        System.out.println(value);
        Double valueTo = (Double) value.get(currencyTo.toString());
        Double convertedAmt = valueTo * quantity;
        System.out.println(convertedAmt);
        //model.addAttribute("textField", convertedAmt.toString());
        amount = convertedAmt.toString();
        monetary.setAmount(amount);
        //model.addAttribute("placeholder2", convertedAmt.toString());

        return "index";
    }

    @NotNull
    @Cacheable("currencies.json")
    private Response getResponse(CurrencyFrom currencyFrom) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://api.exchangeratesapi.io/latest?base="+currencyFrom.toString())
                .method("GET", null)
                .addHeader("Cookie", "__cfduid=d3d102895b210a66dec04c029fb472f1e1600959146")
                .build();
        return client.newCall(request).execute();
    }
}
