package de.c24.finacc.klt.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.Entity;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class Monetary {
    public CurrencyFrom getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(CurrencyFrom currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public CurrencyTo getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(CurrencyTo currencyTo) {
        this.currencyTo = currencyTo;
    }

    public String getTfCurrencyFrom() {
        return tfCurrencyFrom;
    }

    public void setTfCurrencyFrom(String tfCurrencyFrom) {
        this.tfCurrencyFrom = tfCurrencyFrom;
    }

    public String getTfCurrencyTo() {
        return tfCurrencyTo;
    }

    public void setTfCurrencyTo(String tfCurrencyTo) {
        this.tfCurrencyTo = tfCurrencyTo;
    }

    private CurrencyFrom currencyFrom;
    private CurrencyTo currencyTo;
    private String tfCurrencyFrom;
    private String tfCurrencyTo;

    public String getTfCurrencyConversion() {
        return tfCurrencyConversion;
    }

    public void setTfCurrencyConversion(String tfCurrencyConversion) {
        this.tfCurrencyConversion = tfCurrencyConversion;
    }

    private String tfCurrencyConversion;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    private String amount;
}
